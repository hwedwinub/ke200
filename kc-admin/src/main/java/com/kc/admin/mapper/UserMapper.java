package com.kc.admin.mapper;


import com.kc.admin.model.User;
import org.apache.ibatis.annotations.Param;



/**
 * author shish
 * Create Time 2019/1/12 14:07
 * author email shisheng@live.com
 * website www.bangnila.com
 **/

public interface UserMapper  {
    /**
     * 通过用户名查询用户信息
     * @param userName
     * @return
     */
    User findByUserName(String userName);

    /**
     * 添加用户
     * @param user
     * @return
     */
    int insert(User user);

    /**
     * 根据用户名删除用户信息
     * @param username
     * @return
     */
    int del(@Param("username") String username);

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    int update(User user);
    int insertUserInfo(User userInfo);
}
