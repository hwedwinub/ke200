package com.kc.platform.Mapper;

import com.kc.platform.model.TokenEntity;
import org.apache.ibatis.annotations.Param;

/**
 * author shish
 * Create Time 2019/1/11 18:02
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface ApiTokenMapper {
    TokenEntity queryByTokenUserId(long userId);

    void update(TokenEntity tokenEntity);

    void save(TokenEntity tokenEntity);
    TokenEntity queryByToken(@Param("token") String token);
}
