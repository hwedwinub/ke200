package com.mybatisplus.kc.controller;

import com.mybatisplus.kc.common.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * author shish
 * Create Time 2019/1/12 17:13
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("/admin")
public class ApiAdminMapInfo {

    @RequestMapping("/add")
    @RequiresPermissions("user:userInfo:add")
    public R add(){
        return R.ok("add可以访问");
    }

    @RequestMapping("/view")
    @RequiresPermissions("user:userInfo:view")
    public R view(){
        return R.ok("view可以访问");
    }


}
