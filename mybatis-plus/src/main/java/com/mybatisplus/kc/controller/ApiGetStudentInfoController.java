package com.mybatisplus.kc.controller;

import com.mybatisplus.kc.abstractController.AbstractController;
import com.mybatisplus.kc.common.R;
import com.mybatisplus.kc.model.StudentEntity;
import com.mybatisplus.kc.service.StudentService;
import com.mybatisplus.kc.utils.excel.POIUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/4/29 10:55
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("student")
public class ApiGetStudentInfoController extends AbstractController {


    @Autowired
    private StudentService studentService;
    /**
     * Description: 只能获取他自己添加的学生信息
     * Param: []
     * return: com.mybatisplus.kc.common.R
     * Author: shish
     * Date: 2019/4/29
     */
    @RequestMapping("/list")
    public R getList(String class_grade) {
        Integer uid=getUserId();
        List<StudentEntity> studentEntityList =studentService.selectList(class_grade,uid);
        Map<String,Object> map=new HashMap<>();
        map.put("list",studentEntityList);
        return R.ok(map);
    }
    @RequestMapping("/import")
    public  R ExcelImport(MultipartFile file) throws IOException {
        List<String[]> userList=null;
        StudentEntity studentEntity=new StudentEntity();
           userList= POIUtil.readExcel(file);
            for (int i=0;i<userList.size();i++){
                String[] student=userList.get(i);
                StudentEntity stu=studentService.selectOne(Long.parseLong(student[0]));
               if (stu==null){
                   studentEntity.setUserId(Long.parseLong(student[0]));
                   studentEntity.setPassword(DigestUtils.sha256Hex(student[1]));
                   studentEntity.setName(student[2]);
                   studentEntity.setClass_grade(student[3]);
                   studentEntity.setGrade(student[4]);
                   studentEntity.setTel(student[5]);
                   studentEntity.setSex(Integer.parseInt(student[6]));
                   studentEntity.setNickname(student[7]);
                   studentEntity.setPer_sign(student[8]);
                   studentEntity.setEmail(student[9]);
                   studentEntity.setUrl(student[10]);
                   studentEntity.setQuestion(student[11]);
                   studentEntity.setAnswer(student[12]);
                   System.out.println(student[0]);
                   studentService.save(studentEntity);
               }
            }
        //导入学生信息
        Map<String,Object> map=new HashMap<>();
        map.put("remark","导入成功");
        return R.ok(map);
    }

}
