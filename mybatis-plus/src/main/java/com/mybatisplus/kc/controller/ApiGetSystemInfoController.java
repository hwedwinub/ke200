package com.mybatisplus.kc.controller;

import com.mybatisplus.kc.abstractController.AbstractController;
import com.mybatisplus.kc.common.R;
import com.mybatisplus.kc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/4/30 19:42
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("system")
public class ApiGetSystemInfoController extends AbstractController {
    @Autowired
    private Environment environment;

    @RequestMapping("/info")
    public R getSystem(){
        User user=getUser();
        Map<String, Object> result = new HashMap<>(4);
        result.put("env", environment);
        result.put("user",user.getName());
        return R.ok(result);
    }
}
