package com.mybatisplus.kc.controller;

import com.mybatisplus.kc.abstractController.AbstractController;
import com.mybatisplus.kc.common.R;
import com.mybatisplus.kc.model.MenuEntity;
import com.mybatisplus.kc.service.MenuService;
import com.mybatisplus.kc.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/3/8 9:48
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("sys")
public class ApiMenuController extends AbstractController {
    @Autowired
    private MenuService menuService;
    //根据角色获取到菜单
    @RequestMapping("/menu")
    @RequiresPermissions("user:admin:select:add")
    public R SysManager(){
        Integer userId=getUserId();
        List<MenuEntity> menuEntityList=menuService.getMenuByUserId(userId);
        Map<String,Object> map =new HashMap<>();
        map.put("list",menuEntityList);
        return R.ok(map);
    }
}
