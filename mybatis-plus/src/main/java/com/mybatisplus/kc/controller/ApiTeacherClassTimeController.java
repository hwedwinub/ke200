package com.mybatisplus.kc.controller;

import com.mybatisplus.kc.abstractController.AbstractController;
import com.mybatisplus.kc.common.R;
import com.mybatisplus.kc.model.ClassTimeEntity;
import com.mybatisplus.kc.model.DeptClassTeacherCourse;
import com.mybatisplus.kc.service.lmp.ClassTimeServicelmp;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * author shish
 * Create Time 2019/3/8 15:23
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("class")
public class ApiTeacherClassTimeController extends AbstractController {
    @Autowired
    private ClassTimeServicelmp classTimeServicelmp;

    @RequestMapping("/add")
    public  R addClassSignInfo( DeptClassTeacherCourse classTimeEntity){
        Integer code=0;
       try {
           Integer uid=getUserId();
           classTimeEntity.setUid(uid);
           classTimeServicelmp.add(classTimeEntity);
           code=1;
       }catch (Exception e){
       logger.info("错误信息》》》》》》》》》》》》》》》》"+e);
       code=0;
       }
        Map<String,Object> map=new HashMap<>();
        map.put("code",code);
        return R.ok(map);
    }
    @RequestMapping("/update")
    public  R update(DeptClassTeacherCourse classTimeEntity){
        Integer code=0;
        try {
            Integer uid=getUserId();
            classTimeEntity.setUid(uid);
            classTimeServicelmp.update(classTimeEntity);
            code=1;
        }catch (Exception e){
            logger.info("错误信息》》》》》》》》》》》》》》》》"+e);
            code=0;
        }
        Map<String,Object> map=new HashMap<>();
        map.put("code",code);
        return R.ok(map);
    }
    @RequestMapping("/del")
    public Integer del(Integer id){
        Integer code=0;
        try {
            classTimeServicelmp.del(id);
            code=1;
        }catch (Exception e){
            logger.info("错误信息》》》》》》》》》》》》》》》》"+e);
            code=0;
        }
        return code;
    };

    @RequestMapping("/alldel")
    public Integer Alldel(Integer[] ids){
        Integer code=0;
        try {
           for (Integer id:ids){
               classTimeServicelmp.del(id);
           }
            code=1;
        }catch (Exception e){
            logger.info("错误信息》》》》》》》》》》》》》》》》"+e);
            code=0;
        }
        return code;
    };
  @RequestMapping("/list")
    public R list(){
      Integer uid=getUserId();
      logger.info("获取打卡系统登陆>>>>>>>>>"+uid);
      List<DeptClassTeacherCourse> classTimeEntityList=classTimeServicelmp.queryList(uid);
      Map<String,Object> result=new HashMap<>();
      result.put("list",classTimeEntityList);
      return R.ok(result);
    }


    @RequestMapping("/obj")
    public R object(Integer id){
        Integer uid=getUserId();
        DeptClassTeacherCourse classTimeEntity=classTimeServicelmp.QueryObject(id);
        Map<String,Object> result=new HashMap<>();
        result.put("obj",classTimeEntity);
        return R.ok(result);
    }

    //只需要加上下面这段即可，注意不能忘记注解
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {

        //转换日期
        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));// CustomDateEditor为自定义日期编辑器
    }

}
