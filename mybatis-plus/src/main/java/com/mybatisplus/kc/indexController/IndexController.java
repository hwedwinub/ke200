package com.mybatisplus.kc.indexController;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;

/**
 * author shish
 * Create Time 2019/1/12 14:24
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Controller
public class IndexController {
    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    @RequestMapping("/notLogin")
    public  String  login(){
        return "login";
    }
    @RequestMapping("/")
    public  String  ind(){
        return "index";
    }

    @RequestMapping("/index")
    public  String  index(){
        return "index";
    }


    @RequestMapping("/welcome.html")
    public  String  welcome(){
        return "welcome";
    }


    @RequestMapping("/admin-add.html")
    public  String  admin_add(){
        return "admin-add";
    }


    @RequestMapping("/admin-cate.html")
    public  String  admin_cate(){
        return "admin-cate";
    }


    @RequestMapping("/admin-edit.html")
    public  String  admin_edit(){
        return "admin-edit";
    }

    @RequestMapping("/admin-list.html")
    public  String admin_list(){
        return "admin-list";
    }


    @RequestMapping("/admin-role.html")
    public  String admin_role(){
        return "admin-role";
    }

    @RequestMapping("/admin-rule.html")
    public  String admin_rule(){
        return "admin-rule";
    }

    //打卡管理
    @RequestMapping("/order-list.html")
    public  String   order_list(){
        return "lesson-list";
    }


    @RequestMapping("/delCookie")
    public String logout(HttpServletRequest request ,final  Jedis jedis) {
        SecurityUtils.getSubject().logout(); // session删除、RememberMe cookie

        // 也将被删除
        return "login";
    }

//    @RequestMapping("/notRole")
//    public String notRole() {
//        return "404";
//    }
//    @RequestMapping("/favicon.ico")
//   public  String  favicon(){
//        return "static/favicon.ico";
//   }
}
