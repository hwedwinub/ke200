package com.mybatisplus.kc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mybatisplus.kc.model.CourseEntity;
import com.mybatisplus.kc.model.DeptCourseEntity;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 14:37
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface CourseMapper extends BaseMapper<CourseEntity> {

    CourseEntity testquery();
    List<DeptCourseEntity> selectListByUerId(Integer uid);
}
