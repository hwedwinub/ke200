package com.mybatisplus.kc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mybatisplus.kc.model.MenuEntity;

/**
 * author shish
 * Create Time 2019/3/7 16:17
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface TestMapper extends BaseMapper<MenuEntity> {
    String test();
}
