package com.mybatisplus.kc.service;

import com.mybatisplus.kc.model.MenuEntity;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 9:45
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface MenuService {
    List<MenuEntity> getMenuByUserId(Integer userId);
}
